package com.trentv4.pliable;

import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_RGBA8;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

/** Class used to wrap OpenGL's method of handling textures. */
public class Texture
{
	private static boolean doAlert = true;

	/** OpenGL texture ID */
	public int id;
	/** Width of texture in pixels. */
	public int width;
	/** Height of texture in pixels. */
	public int height;
	/** Name of the texture, used in search functions. */
	public String name;

	/** Privately used constructor, used during loadTexture(). */
	private Texture(int id, int width, int height)
	{
		this.id = id;
		this.width = width;
		this.height = height;
	}

	private static HashMap<String, Texture> textureMap;

	public static final void init()
	{
		textureMap = new HashMap<String, Texture>();
	}

	/**
	 * Loads and returns a custom Texture from <i>path</i>. This is checked
	 * against a HashMap containing all previously-loaded images to prevent
	 * unnecessary disk usage.
	 */
	public static final Texture loadTexture(String path)
	{
		if (textureMap.containsKey(path))
		{
			return textureMap.get(path);
		}
		BufferedImage image = null;
		try
		{
			image = ImageIO.read(new File(MainGame.getPath() + "textures/" + path));
			if (doAlert)
				Logger.log(LogLevel.NOTE, "Loaded texture: " + path);
		} catch (Exception e)
		{
			try
			{
				image = ImageIO.read(new File(MainGame.getPath() + "textures/notexture.png"));
				if (doAlert)
					Logger.log(LogLevel.NOTE, "Unable to load texture: " + path);
			} catch (IOException e1)
			{
				MainGame.crash(new Exception("Unable to load both " + path + " texture and unable to load notexture.png!"));
				return null;
			}
		}
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);

		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++)
			{
				int pixel = pixels[y * image.getWidth() + x];
				// Add the RED component
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				// Add the GREEN component
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				// Add the BLUE component
				buffer.put((byte) (pixel & 0xFF));
				// Add the ALPHA component
				buffer.put((byte) ((pixel >> 24) & 0xFF));
			}
		}
		buffer.flip();

		int textureID = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		Texture returner = new Texture(textureID, image.getWidth(), image.getHeight());
		returner.name = path;
		textureMap.put(path, returner);
		return returner;
	}

	public static final void setLoadAlerts(boolean state)
	{
		doAlert = state;
	}
}