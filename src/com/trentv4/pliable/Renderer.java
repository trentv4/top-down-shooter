package com.trentv4.pliable;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;

public class Renderer
{
	public Renderer()
	{
	}

	public void drawImage(String id, double x, double y, double xSize, double ySize)
	{
		drawImage(id, x, y, xSize, ySize, 0);
	}

	public void drawImage(Texture texture, Color color, double x, double y, double xSize, double ySize)
	{
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture.id);
		glColor4f(color.getRed(), color.getGreen(), color.getBlue(), 1);
		glTranslated(x, y, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	public void drawImage(String id, double x, double y, double xSize, double ySize, double angle)
	{
		Texture texture = Texture.loadTexture(id);
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture.id);
		glColor4f(127.5F, 127.5F, 127.5F, 1);
		glTranslated(x + (xSize / 2), y - (ySize / 2), 0);
		glRotated(angle, 0.0F, 0F, 1F);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(-xSize / 2, -ySize / 2);
		glTexCoord2f(1, 0);
		glVertex2d(xSize / 2, -ySize / 2);
		glTexCoord2f(0, 1);
		glVertex2d(-xSize / 2, ySize / 2);
		glTexCoord2f(1, 1);
		glVertex2d(xSize / 2, ySize / 2);
		glEnd();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	public void drawRectangle(Color color, double x, double y, double xSize, double ySize)
	{
		glPushMatrix();
		glColor4f(color.getRed(), color.getGreen(), color.getBlue(), 1);
		glTranslated(x, y, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
	}
	
	public void drawRectangle(Color color, double x, double y, double xSize, double ySize, float alpha)
	{
		glPushMatrix();
		glColor4f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue(), alpha);
		glTranslated(x, y, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
	}

	public void drawText(String text, Color color, int x, int y, int xSize)
	{
		char[] a = text.toLowerCase().toCharArray();
		int xpos = x;
		for (int i = 0; i < a.length; i++)
		{
			Texture texture;
			if (a[i] == '.')
				texture = Texture.loadTexture("lang/period.png");
			else if (a[i] == '/')
				texture = Texture.loadTexture("lang/slash.png");
			else if (a[i] == ':')
				texture = Texture.loadTexture("lang/colon.png");
			else if (a[i] == '\'')
				texture = Texture.loadTexture("lang/apos.png");
			else if (a[i] == '!')
				texture = Texture.loadTexture("lang/excl.png");
			else if (a[i] == '+')
				texture = Texture.loadTexture("lang/plus.png");
			else if (a[i] == ' ')
			{
				xpos += xSize * 2;
				continue;
			} else
			{
				texture = Texture.loadTexture("lang/" + Character.toString(a[i]) + ".png");
			}
			drawImage(texture, color, xpos, y, texture.width * xSize, texture.height * xSize);
			xpos += (texture.width * xSize) + xSize;
		}
	}

}
