package com.trentv4.pliable;

import java.io.File;

import com.trentv4.game.settings.Properties;

/**
 * Main class of the program. This handles the life status of the program, all
 * tracked threads, stores the program directory, and provides several useful
 * methods in the the general execution of the program.
 */
public class MainGame
{
	private static boolean isRunning = false;
	private static MainGame theGame;
	private static String path;
	public static String version = "1.0 RELEASE";
	private static final int TICK_RATE = 120;

	/** Main entry point of the program. Accepts no arguments. */
	public static void main(String[] args)
	{
		Logger.initialize(LogLevel.INIT_NOTE);
		try
		{
			theGame = new MainGame();
			path = new File("").getCanonicalPath() + "/"; // fix me.
			Logger.log(LogLevel.INIT_NOTE, "Created path at " + path);
			Texture.init();
			isRunning = true;
			Thread t = new Thread(theGame.new GameThread());
			t.setName("Game Thread");
			t.start();
			Logger.log(LogLevel.INIT_NOTE, "Game thread started.");
			new Properties("settings.txt");
			DisplayManager.initialize();
		} catch (Exception e)
		{
			crash(e);
		}
	}

	/** Kills the program. */
	public static void kill()
	{
		Logger.log("Program is being killed.");
		isRunning = false;
		Logger.saveLog("log.txt");
		System.exit(0); // I ain't havin' no leakin' threads on my watch!
	}

	/** Prints the Exception and then kills the program. */
	public static final void crash(Exception e)
	{
		Logger.log("Program has crashed! Printing error log: ");
		e.printStackTrace();
		kill();
	}

	/**
	 * Checks the status of the program (if executing or not). This allows for
	 * safe, <i>ConcurrentModificationException</i>-free heartbeat checking.
	 */
	public static boolean isAlive()
	{
		return isRunning;
	}

	/**
	 * Returns the current directory, ready-formatted for use, e.g.
	 * "<i>/home/trent/execution_folder/</i>".
	 */
	public static String getPath()
	{
		return path;
	}

	private class GameThread implements Runnable
	{
		@Override
		public void run()
		{
			long resolution = 1000000000 / TICK_RATE;
			long startTime = System.nanoTime();
			long currentTime = startTime;
			while (isRunning)
			{
				currentTime = System.nanoTime();
				if (currentTime - startTime > resolution)
				{
					startTime = System.nanoTime();
					currentTime = startTime;
					GameLoop.run();
				}
			}
		}
	}
}
