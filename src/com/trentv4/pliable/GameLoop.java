package com.trentv4.pliable;

import java.util.Stack;

import com.trentv4.game.core.GameStructureGameplay;

/**
 * Primary loop class for the program. This class only exposes two methods:
 * run() and draw(), called from MainGame and DisplayManager each.
 */
public final class GameLoop
{
	public static Stack<GameStructure> gameStack = new Stack<GameStructure>();
	private static Renderer renderer = new Renderer();
	
	public static GameStructureGameplay s;

	/**
	 * Executes one iteration of the game loop, or initializes it if it hasn't
	 * been.
	 */
	public static final void run()
	{
		if (!gameStack.empty())
		{
			GameStructure top = gameStack.peek();
			if (!top.isInitialized)
			{
				top.initialize();
				top.isInitialized = true;
			}
			if (DisplayManager.isInitialized())
			{
				top.tick();
				if (top.getInputScenario() != null)
				{
					top.getInputScenario().tick();
				}
			}
		} else
		{
			s = new GameStructureGameplay();
			gameStack.add(s);
			// add gameStacks here
		}
	}

	/** Exposed draw function, called from the DisplayManager class. */
	public static final void draw()
	{
		if (!gameStack.empty())
		{
			GameStructure[] structures = gameStack.toArray(new GameStructure[gameStack.size()]);
			for (int i = 0; i < structures.length; i++)
			{
				structures[i].draw(renderer);
			}
		}
	}
}
