package com.trentv4.game.core;

import static com.trentv4.pliable.InputMapper.getMouseButtons;
import static com.trentv4.pliable.InputMapper.getMousePos;
import static com.trentv4.pliable.InputMapper.isDown;
import static com.trentv4.pliable.InputMapper.isPressed;
import static com.trentv4.pliable.GameLoop.s;

import org.lwjgl.glfw.GLFW;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.InputScenario;

public class InputScenarioGameplay extends InputScenario
{
	public void tick()
	{
		s.player.angle = Math.toDegrees(Math.atan2(getMousePos()[1] + 20 - DisplayManager.height/2, getMousePos()[0] - 20 - DisplayManager.width/2));
		if (getMouseButtons()[0])
		{
			s.player.weapon.fire(s.player, s.player.angle);
		}
		if (isPressed(GLFW.GLFW_KEY_R))
		{
			s.player.weapon.reload_delay = s.player.weapon.max_reload_delay;
		}
		if (isPressed(GLFW.GLFW_KEY_SPACE))
		{
			System.out.println(s.player.angle);
		}
		if (isPressed(GLFW.GLFW_KEY_F))
		{
			if(s.player.stim > 0)
			{
				if(s.player.hp != s.player.MAX_HP)
				{
					s.player.stim--;
					s.player.hp += 50;
					if(s.player.hp > 250)
					{
						s.player.hp = 250;
					}
				}
			}
		}
		if (isDown(GLFW.GLFW_KEY_W))
		{
			s.player.yVelocity -= s.player.speed;
		}
		if (isDown(GLFW.GLFW_KEY_A))
		{
			s.player.xVelocity -= s.player.speed;
		}
		if (isDown(GLFW.GLFW_KEY_S))
		{
			s.player.yVelocity += s.player.speed;
		}
		if (isDown(GLFW.GLFW_KEY_D))
		{
			s.player.xVelocity += s.player.speed;
		}
		if (isPressed(GLFW.GLFW_KEY_1))
		{
			s.player.texture = ("player_sawedoff.png");
			s.player.weapon = s.player.shotgun;
		}
		if (isPressed(GLFW.GLFW_KEY_2))
		{
			s.player.texture = ("player_assault.png");
			s.player.weapon = s.player.assault;
		}
		if (isPressed(GLFW.GLFW_KEY_3))
		{
			s.player.texture = ("player_pistol.png");
			s.player.weapon = s.player.pistol;
		}

	}
}
