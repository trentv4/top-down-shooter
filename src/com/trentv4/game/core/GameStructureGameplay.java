package com.trentv4.game.core;

import java.awt.Color;
import java.util.ArrayList;

import com.trentv4.game.content.Entity;
import com.trentv4.game.content.EntityEnemy;
import com.trentv4.game.content.EntityEnemyBoss;
import com.trentv4.game.content.EntityPlayer;
import com.trentv4.game.content.Terrain;
import com.trentv4.game.content.Weapon;
import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.Renderer;

public class GameStructureGameplay extends GameStructure 
{
	public EntityPlayer player;
	public EntityEnemyBoss boss;
	private Terrain[][] map;
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	public Weapon[] weapons;	
	
	public int cameraX = 0;
	public int cameraY = 0;
	
	private float fade = 0;
	
	public boolean hasSeenBoss = false;
	
	public void initialize()
	{
		scenario = new InputScenarioGameplay();
		//				capacity, reload time, fire time, damage
		weapons = new Weapon[]{
				new Weapon("shotgun", 5, 100, 150, 13),         //shotgun
				new Weapon("assault", 30, 500, 5, 15),          //assault rifle
				new Weapon("pistol", 6, 750, 150, 100),         //revolver
				new Weapon("dualassault", 60, 300, 5, 10),      //dual-AKs (boss)
				new Weapon("debug", 300000000, 0, 0, 0)         //debug gun
				};
		
		//boss & player
		player = new EntityPlayer("player_sawedoff.png", 3837, 513, 40, 40, 250,1); player.weapon = player.shotgun;
		boss = new EntityEnemyBoss("boss.png", 10, 700, 80, 80, 1000, 1); boss.setWeapon(weapons[3]);
		
		//enemies
		new EntityEnemy("enemy_assault.png", 569, 184, 40, 40, 100,1).setWeapon(weapons[1]);
		
		new EntityEnemy("enemy2_assault.png", 779, 385, 40, 40, 100,1).setWeapon(weapons[1]).angle=100;
		new EntityEnemy("enemy_pistol.png", 586, 385, 40, 40, 100,1).setWeapon(weapons[2]).angle=90;
		new EntityEnemy("enemy_pistol.png", 791, 480, 40, 40, 100,1).setWeapon(weapons[2]).angle=160;
		
		new EntityEnemy("enemy2_sawedoff.png", 577, 708, 40, 40, 100,1).setWeapon(weapons[0]);
		new EntityEnemy("enemy_assault.png", 1043, 822, 40, 40, 100,1).setWeapon(weapons[1]).angle=-80;
		new EntityEnemy("enemy2_sawedoff.png", 1267, 614, 40, 40, 100,1).setWeapon(weapons[0]).angle=-90;
		new EntityEnemy("enemy2_assault.png", 1267, 381, 40, 40, 100,1).setWeapon(weapons[1]).angle=-90;
		new EntityEnemy("enemy2_pistol.png", 1731, 185, 40, 40, 100,1).setWeapon(weapons[2]).angle=80;
		new EntityEnemy("enemy_sawedoff.png", 1731, 375, 40, 40, 100,1).setWeapon(weapons[0]).angle=45;
		new EntityEnemy("enemy_sawedoff.png", 2175, 496, 40, 40, 100,1).setWeapon(weapons[0]).angle=90;
		new EntityEnemy("enemy2_assault.png", 1969, 716, 40, 40, 100,1).setWeapon(weapons[1]);
		new EntityEnemy("enemy_pistol.png", 2601, 703, 40, 40, 100,1).setWeapon(weapons[2]).angle=-90;
		new EntityEnemy("enemy2_pistol.png", 2865, 834, 40, 40, 100,1).setWeapon(weapons[2]).angle=-150;
		new EntityEnemy("enemy_assault.png", 2637, 367, 40, 40, 100,1).setWeapon(weapons[1]).angle=-90;
		new EntityEnemy("enemy_assault.png", 2437, 187, 40, 40, 100,1).setWeapon(weapons[1]);
		new EntityEnemy("enemy2_sawedoff.png", 3065, 293, 40, 40, 100,1).setWeapon(weapons[0]).angle=75;
		
		
		//container values (easier to modify)
		int scale = 3;
		int container_x = scale * 84;
		int container_y = scale * 42;
		int container_x_pos = container_x - scale * 7;
		int container_y_pos = container_y - scale * 7;
		
		//begin mapping		
		map = new Terrain[][] 
		{
			{
				new Terrain("ship.png", -538, -137, 1568*scale, 404*scale-1, 0).setSolid(true)
			},
			{
				new Terrain("invisible", -500, 0, 6000, 40, 1).setSolid(true),
				new Terrain("invisible", -500, 950, 6000, 40, 1).setSolid(true),
				new Terrain("invisible", -150, 0, 40, 950, 1).setSolid(true),
				new Terrain("invisible", 3900, -20, 40, 1000, 1).setSolid(true),
				new Terrain("container_green.png",container_x_pos*0, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*1, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*2, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*3, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*4, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*5, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*6, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*7, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*8, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*9, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*10, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*11, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*12, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*13, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*14, container_y_pos*0, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*0, container_y_pos*1, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*4, container_y_pos*1, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*8, container_y_pos*1, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*9, container_y_pos*1, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*14, container_y_pos*1, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*2, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*4, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*6, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*9, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*12, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*14, container_y_pos*2, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*0, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*1, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*4, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*6, container_y_pos*3, container_x, container_y, 1).setSolid(true), /*TODO*/ new Terrain("container_blue.png",container_x_pos*9, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*12, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*14, container_y_pos*3, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*1, container_y_pos*4, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*4, container_y_pos*4, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*6, container_y_pos*4, container_x, container_y, 1).setSolid(true), /*TODO*/ new Terrain("container_grey.png",container_x_pos*10, container_y_pos*4, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*12, container_y_pos*4, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*1, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*3, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*7, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*8, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*10, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*13, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*14, container_y_pos*5, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*1, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*5, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*7, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*10, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*13, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*14, container_y_pos*6, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*1, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*2, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*3, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*7, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*8, container_y_pos*7, container_x, container_y, 1).setSolid(true), /*TODO*/ new Terrain("container_red.png",container_x_pos*13, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*14, container_y_pos*7, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*0, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*1, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*2, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*3, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*4, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*5, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*6, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*7, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*8, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*9, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_green.png",container_x_pos*10, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_grey.png",container_x_pos*11, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*12, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_blue.png",container_x_pos*13, container_y_pos*8, container_x, container_y, 1).setSolid(true), new Terrain("container_red.png",container_x_pos*14, container_y_pos*8, container_x, container_y, 1).setSolid(true),
			},
			{
				new Terrain("container_green.png",container_x_pos*0 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*2 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*3 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*4 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*6 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*8 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*9 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*10 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*11 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*12 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*13 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*0 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*4 - (scale * 6 * 1), container_y_pos*1 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*8 - (scale * 6 * 1), container_y_pos*1 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*9 - (scale * 6 * 1), container_y_pos*1 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*1 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*2 - (scale * 6 * 1), container_y_pos*2 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*9 - (scale * 6 * 1), container_y_pos*2 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*12 - (scale * 6 * 1), container_y_pos*2 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*2 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*0 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*4 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*6 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*9 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*3 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*4 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*4 - (scale * 6 * 1), container_y_pos*4 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*6 - (scale * 6 * 1), container_y_pos*4 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*10 - (scale * 6 * 1), container_y_pos*4 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*12 - (scale * 6 * 1), container_y_pos*4 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*3 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*7 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*10 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*13 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*5 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*5 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*7 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*10 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*13 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*6 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*2 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*3 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*7 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*8 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*13 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*7 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*0 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*1 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*2 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*3 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*5 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*6 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*8 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_green.png",container_x_pos*9 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*10 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_blue.png",container_x_pos*11 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_grey.png",container_x_pos*12 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),new Terrain("container_red.png",container_x_pos*14 - (scale * 6 * 1), container_y_pos*8 - (scale * 6 * 1), container_x, container_y, 2).setSolid(true),
			},
			{
				new Terrain("container_green.png",container_x_pos*0 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*2 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*3 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*4 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*6 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*8 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*9 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*12 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*13 - (scale * 6 * 2), container_y_pos*0 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*4 - (scale * 6 * 2), container_y_pos*1 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*8 - (scale * 6 * 2), container_y_pos*1 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*9 - (scale * 6 * 2), container_y_pos*1 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*1 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*2 - (scale * 6 * 2), container_y_pos*2 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*9 - (scale * 6 * 2), container_y_pos*2 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*12 - (scale * 6 * 2), container_y_pos*2 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*2 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*0 - (scale * 6 * 2), container_y_pos*3 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*6 - (scale * 6 * 2), container_y_pos*3 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*9 - (scale * 6 * 2), container_y_pos*3 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*3 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*4 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*4 - (scale * 6 * 2), container_y_pos*4 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*6 - (scale * 6 * 2), container_y_pos*4 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*10 - (scale * 6 * 2), container_y_pos*4 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*12 - (scale * 6 * 2), container_y_pos*4 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*5 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*3 - (scale * 6 * 2), container_y_pos*5 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*7 - (scale * 6 * 2), container_y_pos*5 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*10 - (scale * 6 * 2), container_y_pos*5 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*13 - (scale * 6 * 2), container_y_pos*5 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*6 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*10 - (scale * 6 * 2), container_y_pos*6 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*13 - (scale * 6 * 2), container_y_pos*6 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*6 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*2 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*3 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*7 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*8 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*13 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*7 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*1 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*2 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*3 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*5 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*6 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*8 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_red.png",container_x_pos*9 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_blue.png",container_x_pos*10 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_grey.png",container_x_pos*11 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),new Terrain("container_green.png",container_x_pos*14 - (scale * 6 * 2), container_y_pos*8 - (scale * 6 * 2), container_x, container_y, 3).setSolid(true),
			}
		};
	}
	
	public void tick()
	{
		for (int i = 0; i < entities.size(); i++)
		{
			Entity e = entities.get(i);
			if (!e.isWaitingForCleanup)
			{
				if(e.weapon != null)
				{
					e.weapon.tick();
				}
				e.tick();
				e.applyPhysics();

			} else
			{
				entities.remove(i);
			}
		}
		if(Entity.canSee(player, boss.x, boss.y))
		{
			hasSeenBoss = true;
		}
		cameraX = -(-DisplayManager.width/2 + (int) player.x);
		cameraY = -(-DisplayManager.height/2 + (int) player.y);

	}
	
	int water_move = 0;
	int water_delay = 10;
	public void draw(Renderer renderer)
	{
		renderer.drawImage("water.png", 0+water_move-640*3, DisplayManager.height, 640*3, 360*3);
		renderer.drawImage("water.png", 0+water_move, DisplayManager.height, 640*3, 360*3);
		water_delay--;
		if(water_delay == 0)
		{
			water_delay = 10;
			water_move++;
			if(water_move >= 640*3)
			{
				water_move = 0;
			}
		}

		Terrain[][] newMap = getMap();
		for (int level = 0; level < newMap.length; level++)
		{
			for (int tile = 0; tile < newMap[level].length; tile++)
			{
				newMap[level][tile].render(renderer);
			}
		}
		Entity[] b = getEntities();
		for (int i = 0; i < b.length; i++)
		{
			if(b[i] != null)
			{
				b[i].render(renderer);
			}
		}
		renderer.drawImage("shotgun.png", 0, 120, 240, 120);
		renderer.drawImage("assault.png", 240, 120, 240, 120);
		renderer.drawImage("revolver.png", 480, 140, 180, 120);
		renderer.drawImage("stim.png", 660, 120, 120, 120);
		renderer.drawText(player.shotgun.magazine + "/" + player.shotgun.magazine_max, Color.white, 100, 75, 4);
		renderer.drawText(player.assault.magazine + "/" + player.assault.magazine_max, Color.white, 400, 75, 4);
		renderer.drawText(player.pistol.magazine + "/" + player.pistol.magazine_max, Color.white, 600, 75, 4);
		renderer.drawText(""+player.stim, Color.white, 715, 50, 4);

		renderer.drawText("Health: ", Color.white, 10, DisplayManager.height - 50, 4);
		renderer.drawText(player.hp + "/" + player.MAX_HP, Color.white, 10, DisplayManager.height - 25, 4);
		if(hasSeenBoss)
		{
			renderer.drawText("BOSS HEALTH: ", Color.red, 10, DisplayManager.height - 150, 4);
			renderer.drawText(boss.hp + "/" + boss.MAX_HP, Color.red, 10, DisplayManager.height - 125, 4);
		}
		renderer.drawRectangle(Color.black, 0, 0, DisplayManager.width, DisplayManager.height, fade/100);
		if(boss.isWaitingForCleanup)
		{
			if(fade < 100f)
			{
				fade += 0.25;
			}
			else
			{
				renderer.drawText("THANK YOU SOLDIER" , Color.white, DisplayManager.width/4+200, DisplayManager.height/2-25, 5);
				
				renderer.drawText("BUT OUR CAPTAIN IS IN ANOTHER SHIP! " , Color.white, DisplayManager.width/4+50, DisplayManager.height/2+25, 5);
			}
		}
	}

	public final Terrain[][] getMap()
	{
		if (map != null)
		{
			return map.clone();
		}
		return new Terrain[][] {};
	}

	public final Entity[] getEntities()
	{
		return entities.toArray(new Entity[entities.size()]);
	}

	public final void removeEntity(Entity e)
	{
		for (int i = 0; i < entities.size(); i++)
		{
			if (e.ID == entities.get(i).ID)
			{
				entities.remove(i);
				return;
			}
		}
	}

	public final void addEntity(Entity e)
	{
		entities.add(e);
	}

}
