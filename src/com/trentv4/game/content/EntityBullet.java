package com.trentv4.game.content;

import com.trentv4.pliable.GameLoop;

public class EntityBullet extends Entity
{
	public int damageDelayPassed = 0;
	public int DAMAGE_DELAY = 75;
	public int damage;
	public int iterations = 8;
	
	public EntityBullet(String texture, int x, int y, int xSize, int ySize, int hp, int level) {
		super(texture, x, y, xSize, ySize, hp, level);
		this.WILL_STOP = false;
		this.xSize = 5;
		this.ySize = 5;
	}
	
	@Override
	public Entity applyPhysics()
	{
		Terrain[] map = GameLoop.s.getMap()[level];
		boolean isColliding = false;
		for (Terrain t : map)
		{
			if(t.level == level)
			{
				if (t.isSolid())
				{
					if (x + xSize + xVelocity > t.x)
					{
						if (x + xVelocity < t.x + t.xSize)
						{
							if (y + yVelocity > t.y - t.ySize)
							{
								if (y - ySize + yVelocity < t.y)
								{
									xVelocity = 0;
									yVelocity = 0;
									isColliding = true;
									if (!WILL_STOP)
									{
										kill();
									}
								}
							}
						}
					}
				}
			}
		}
		if (!isColliding)
		{
			for(int i = 1; i <= iterations; i++)
			{
				x += xVelocity/i;
				y += yVelocity/i;
				tick();
			}
		}
		if (WILL_STOP)
		{
			xVelocity = 0;
			yVelocity = 0;
		}
		return this;
	}
	
	@Override
	public Entity kill()
	{
		return super.kill();
	}
	
	@Override
	public Entity tick()
	{
		damageDelayPassed++;
		Entity[] e = GameLoop.s.getEntities();
		for (int i = 0; i < e.length; i++)
		{
			if(willIntersect(e[i]) & !(e[i] instanceof EntityBullet))
			{
				if (damageDelayPassed >= DAMAGE_DELAY)
				{
					if(!isWaitingForCleanup)
					{
						kill();
						e[i].hp -= damage;
					}
				}
				if (e[i].hp <= 0)
				{
					if (!e[i].isWaitingForCleanup)
					{
						e[i].kill();
					}
				}
			}
		}
		return this;
	}
	
	public boolean willIntersect(Entity e)
	{
		double dist = Math.hypot(e.x - x, e.y - y);
		if(dist < e.xSize)
		{
			return true;
		}
		return false;
	}
}
