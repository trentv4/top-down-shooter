package com.trentv4.game.content;

import static com.trentv4.pliable.GameLoop.*;

public class EntityEnemy extends Entity
{
	public EntityEnemy(String texture, int x, int y, int xSize, int ySize, int hp, int level) {
		super(texture, x, y, xSize, ySize, hp, level);
	}

	private int delay = 0;
	
	@Override
	public Entity tick()
	{
		if(canSee(this, s.player.x, s.player.y) |
				   canSee(this, s.player.x, s.player.y + s.player.ySize) |
				   canSee(this, s.player.x + s.player.xSize, s.player.y + s.player.ySize) |
				   canSee(this, s.player.x + s.player.xSize, s.player.y + s.player.ySize))
		{
			angle = (Math.toDegrees(Math.atan2((s.player.y - (y + 25 - (ySize))), (s.player.x - (x + 50 - (xSize))))));
			if(delay < 75)
			{
				delay++;
			}
			else
			{
				if(weapon != null)
				{
					weapon.fire(this, angle);
				}
			}
		}
		else
		{
			delay = 0;
		}
		return this;
	}
}
