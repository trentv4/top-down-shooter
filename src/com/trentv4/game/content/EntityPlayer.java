package com.trentv4.game.content;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameLoop;
import com.trentv4.pliable.Renderer;

public class EntityPlayer extends Entity
{
	public Weapon shotgun = new Weapon(GameLoop.s.weapons[0]);
	public Weapon assault = new Weapon(GameLoop.s.weapons[1]);
	public Weapon pistol = new Weapon(GameLoop.s.weapons[2]);
	public int stim = 10;
	
	public EntityPlayer(String texture, int x, int y, int xSize, int ySize, int hp, int level) 
	{
		super(texture, x, y, xSize, ySize, hp, level);
	}
	
	@Override
	public void render(Renderer renderer)
	{
		if (!isWaitingForCleanup)
		{
			renderer.drawImage(texture, DisplayManager.width/2, DisplayManager.height/2, xSize, ySize, angle);
		}
	}
}
