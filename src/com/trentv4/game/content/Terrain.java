package com.trentv4.game.content;

import com.trentv4.pliable.GameLoop;
import com.trentv4.pliable.Renderer;

public class Terrain
{
	public int x;
	public int y;
	public int level;
	public int xSize;
	public int ySize;
	public String id;
	private boolean isSolid = true;

	public Terrain(String id, int x, int y, int xSize, int ySize, int level)
	{
		this.x = x;
		this.y = y + ySize;
		this.xSize = xSize;
		this.ySize = ySize;
		this.id = id;
		this.level = level;
	}

	public Terrain setSolid(boolean t)
	{
		isSolid = t;
		return this;
	}

	public boolean isSolid()
	{
		return isSolid;
	}

	public void render(Renderer renderer)
	{
		if(!id.equals("invisible"))
		{
			renderer.drawImage(id, x + GameLoop.s.cameraX, y + GameLoop.s.cameraY, xSize, ySize, 0);
		}
	}
}
