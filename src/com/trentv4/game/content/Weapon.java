package com.trentv4.game.content;

public class Weapon 
{
	public String name;
	public int magazine;
	public int magazine_max;
	public int reload_delay;
	public int max_reload_delay;
	public int fire_delay;
	public int max_fire_delay;
	public int damage;
	
	
	public Weapon(String name, int capacity, int reload_time, int fire_time, int damage)
	{
		this.name = name;
		this.magazine_max = capacity;
		this.magazine = capacity;
		this.max_reload_delay = reload_time;
		this.reload_delay = 0;
		this.fire_delay = 0;
		this.max_fire_delay = fire_time;
		this.damage = damage;
	}
	
	public Weapon(Weapon base)
	{
		this.name = base.name;
		this.magazine_max = base.magazine_max;
		this.magazine = base.magazine_max;
		this.max_reload_delay = base.max_reload_delay;
		this.reload_delay = 0;
		this.fire_delay = 0;
		this.max_fire_delay = base.max_fire_delay;
		this.damage = base.damage;
	}
	
	public void tick()
	{
//		Logger.log("Delay: " + fire_delay +
//					" Reload: " + reload_delay +
//					" Ammo: " + magazine + "/" + magazine_max);
		if(fire_delay > 0)
		{
			fire_delay--;
		}
		if(name.equals("shotgun"))
		{
			if(reload_delay > 0)
			{
				reload_delay--;
				if(reload_delay == 0)
				{
					magazine += 1;
					if(magazine < magazine_max)
					{
						reload_delay = max_reload_delay;
					}
				}
			}
		}
		else
		{
			if(reload_delay > 0)
			{
				reload_delay--;
				if(reload_delay == 0)
				{
					magazine = magazine_max;
				}
			}
		}
	}
	
	public void fire(Entity invoker, double angle)
	{
		double speed = 5;
		if(fire_delay == 0)
		{
			if(name.equals("shotgun"))
			{
				if(magazine > 0)
				{
					int pellets = 9;
					double spread = 3;
					for (int i = -pellets / 2; i <= pellets / 2; i++)
					{
						EntityBullet bullet = new EntityBullet("bullet.png", (int) (invoker.x + (invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
						bullet.xVelocity = speed * Math.cos(Math.toRadians(angle + (i*spread)));
						bullet.yVelocity = speed * Math.sin(Math.toRadians(angle + (i*spread)));
						bullet.damage = damage;
					}
					magazine--;
					fire_delay = max_fire_delay;
					reload_delay = 0;
					if(magazine == 0)
					{
						reload_delay = max_reload_delay;
					}
				}
			}
			else
			{
				if(reload_delay == 0)
				{
					if(magazine > 0)
					{
						if (name.equals("assault"))
						{
							EntityBullet bullet = new EntityBullet("bullet.png", (int) (invoker.x + (invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
							bullet.xVelocity = speed * Math.cos(Math.toRadians(angle + (Math.random() * 14)-7));
							bullet.yVelocity = speed * Math.sin(Math.toRadians(angle + (Math.random() * 14)-7));
							bullet.damage = damage;
						}
						if (name.equals("dualassault"))
						{
							double spread = 5;
							EntityBullet bullet = new EntityBullet("bullet.png", (int) (invoker.x +(invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
							bullet.xVelocity = speed * Math.cos(Math.toRadians(angle+spread + (Math.random() * 14)-7));
							bullet.yVelocity = speed * Math.sin(Math.toRadians(angle+spread + (Math.random() * 14)-7));
							bullet.damage = damage;
							EntityBullet bullet2 = new EntityBullet("bullet.png", (int) (invoker.x +(invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
							bullet2.xVelocity = speed * Math.cos(Math.toRadians(angle-spread + (Math.random() * 14)-7));
							bullet2.yVelocity = speed * Math.sin(Math.toRadians(angle-spread + (Math.random() * 14)-7));
							bullet2.damage = damage;
						}
						else if (name.equals("pistol"))
						{
							EntityBullet bullet = new EntityBullet("bullet.png", (int) (invoker.x + (invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
							bullet.xVelocity = speed * Math.cos(Math.toRadians(angle));
							bullet.yVelocity = speed * Math.sin(Math.toRadians(angle));
							bullet.damage = damage;
						}
						else if (name.equals("debug"))
						{
							EntityBullet bullet = new EntityBullet("bullet.png", (int) (invoker.x + (invoker.xSize / 2)), (int) (invoker.y - (invoker.xSize / 2)), 2, 2, 1, invoker.level);
							bullet.xVelocity = speed * Math.cos(Math.toRadians(angle));
							bullet.yVelocity = speed * Math.sin(Math.toRadians(angle));
							bullet.damage = damage;
						}
						magazine--;
						fire_delay = max_fire_delay;
						if(magazine == 0)
						{
							reload_delay = max_reload_delay;
						}
					}
					else
					{
						reload_delay = max_reload_delay;
					}
				}
			}
		}
	}
}
