package com.trentv4.game.content;

import com.trentv4.pliable.GameLoop;
import com.trentv4.pliable.Renderer;

public class Entity
{
	public String texture;
	public double x;
	public double y;
	public double xSize;
	public double ySize;
	public double angle;
	public Weapon weapon;

	public double xVelocity;
	public double yVelocity;

	public final int ID;
	private static int MAX_ID = 0;

	public int MAX_HP;
	public boolean WILL_STOP;

	public int level;

	public int hp;
	public double speed = 3;
	public boolean isWaitingForCleanup;

	public Entity(String texture, int x, int y, int xSize, int ySize, int hp, int level)
	{
		this.ID = MAX_ID;
		MAX_ID++;
		this.texture = texture;
		this.x = x;
		this.y = y;
		this.xSize = xSize;
		this.ySize = ySize;
		this.angle = 0;
		this.MAX_HP = hp;
		this.hp = hp;
		this.isWaitingForCleanup = false;
		this.WILL_STOP = true;
		this.level = level;
		GameLoop.s.addEntity(this);
	}
	
	public Entity setWeapon(Weapon weapon)
	{
		this.weapon = new Weapon(weapon);
		return this;
	}
	
	public Entity tick()
	{
		return this;
	}

	public Entity applyPhysics()
	{
		Terrain[] map = GameLoop.s.getMap()[level];
		boolean isColliding = false;
		for (Terrain t : map)
		{
			if(t.level == level)
			{
				if (t.isSolid())
				{
					if (x + xSize + xVelocity > t.x)
					{
						if (x + xVelocity < t.x + t.xSize)
						{
							if (y + yVelocity > t.y - t.ySize)
							{
								if (y - ySize + yVelocity < t.y)
								{
									xVelocity = 0;
									yVelocity = 0;
									isColliding = true;
									if (!WILL_STOP)
									{
										kill();
									}
								}
							}
						}
					}
				}
			}
		}
		if (!isColliding)
		{
			x += xVelocity;
			y += yVelocity;
		}
		if (WILL_STOP)
		{
			xVelocity = 0;
			yVelocity = 0;
		}
		return this;
	}
	
	public Entity kill()
	{
		isWaitingForCleanup = true;
		return this;
	}
	
	public static boolean canSee(Entity viewer, double targetX, double targetY)
	{
		double xDist = viewer.x + (viewer.xSize / 2) - targetX;
		double yDist = viewer.y - (viewer.ySize / 2) - targetY;
		if(xDist > 350 | yDist > 350)
		{
			return false;
		}
		int iterations = (int) (Math.hypot(xDist, yDist));
		double addX = xDist / iterations;
		double addY = yDist / iterations;
		
		double currentX = viewer.x + (viewer.xSize / 2);
		double currentY = viewer.y - (viewer.ySize / 2);
		for(int i = 0; i < iterations; i++)
		{
			currentX -= addX;
			currentY -= addY;
			Terrain[] map = GameLoop.s.getMap()[viewer.level];
			for (Terrain t : map)
			{
				if (t.isSolid())
				{
					if (currentX > t.x)
					{
						if (currentX < t.x + t.xSize)
						{
							if (currentY > t.y - t.ySize)
							{
								if (currentY < t.y)
								{
									return false;
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	public void render(Renderer renderer)
	{
		if (!isWaitingForCleanup)
		{
			renderer.drawImage(texture, x + GameLoop.s.cameraX, y + GameLoop.s.cameraY, xSize, ySize, angle);
		}
	}
}