package com.trentv4.game.settings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.LogLevel;
import com.trentv4.pliable.Logger;
import com.trentv4.pliable.MainGame;

/**
 * Main class that is used to track program information. This is mainly used by
 * the DisplayManager. This class handles all fields as private and provides the
 * following accessors:
 * <ul>
 * <li><i>getWidth()</i></li>
 * <li><i>getHeight()</i></li>
 * <li><i>getMaxFPS()</i></li>
 * <li><i>isShowingFPS()</i></li>
 * <li><i>isShowingAdvancedFPS()</i></li>
 * <li><i>getTitle()</i></li>
 * </ul>
 * In addition, this class provides the method <i>saveProperties(Properties,
 * String)</i>, which will encode and save the <i>Properties</i> object to disk
 * at the <i>String</i> location.
 */
public class Properties
{
	private int WIDTH;
	private int HEIGHT;
	private String TITLE;

	/**
	 * Creates a new, default Properties object. This is auto-populated with
	 * generic information.
	 */
	public Properties()
	{
		this.WIDTH = 800;
		this.HEIGHT = 800;
		this.TITLE = "Top-down shooter";
	}

	/**
	 * Creates a new Properties object based on the file located at <i>path</i>.
	 * It is not required to specify the entire path, only the local directory
	 * path, e.g. "<i>settings.txt</i>". NOTE: Not Yet Implemented.
	 */
	public Properties(String path)
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(MainGame.getPath() + "settings/" + path)));
			String[] list = new String[6];
			for (int i = 0; i < list.length; i++)
			{
				list[i] = reader.readLine();
			}
			reader.close();

			DisplayManager.width = Integer.parseInt(list[0].substring(6));
			DisplayManager.height = Integer.parseInt(list[1].substring(7));
			Logger.log("Properties read successfully");
		} catch (Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to load properties file!");
			this.WIDTH = 800;
			this.HEIGHT = 800;
			this.TITLE = "Top-down shooter";
		}
	}

	/**
	 * Saves the provided Properties object to the provided <i>path</i>. It is
	 * not required to specify the entire path, only the local directory path,
	 * e.g. "<i>settings.txt</i>". NOTE: Not Yet Implemented.
	 */
	public static final void saveProperties(Properties properties, String path)
	{
		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MainGame.getPath() + "settings/" + path)));
			writer.write("width=" + properties.WIDTH + System.lineSeparator());
			writer.write("height=" + properties.HEIGHT + System.lineSeparator());
			writer.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			Logger.log(LogLevel.ERROR, "Unable to save properties file!");
		}
	}

	/** Returns the width of the window. */
	public int getWidth()
	{
		return WIDTH;
	}

	/** Returns the height of the window. */
	public int getHeight()
	{
		return HEIGHT;
	}

	/** Returns the String title of the window. */
	public String getTitle()
	{
		return TITLE;
	}

}
