# Game #

This is a basic top-down shooter created by Trent VanSlyke. It's origin is midnight boredom and it's life was bored work in class. It's fun for a little bit of messing around

### How do I get going? ###

* Git clone.
* Add project to Eclipse (.gitignore is only set up for Eclipse).
* Add the LWJGL3 (http://lwjgl.org/) jar and add natives for your system
* Run.


### Whaaaaat? ###

* [Windows 64](http://jamieswhiteshirt.com/trentv4/projects/top-down/win64.zip)
* [Windows 32](http://jamieswhiteshirt.com/trentv4/projects/top-down/win32.zip)
* [Linux 64](http://jamieswhiteshirt.com/trentv4/projects/top-down/linux64.zip)
* [Linux 32](http://jamieswhiteshirt.com/trentv4/projects/top-down/linux32.zip)

### Controls ###

General: 

* WASD: controls
* Spacebar: revive (when dead)

Weapons: 

* 1 : sawed-off shotgun
* 2 : SMG (30 bullets/magazine, reloads when empty)
* 3 : shotgun (tighter spread/fewer bullets)

### Who do I talk to? ###

* Trent VanSlyke. Found here: trentvanslyke@gmail.com, @trentv4, jamieswhiteshirt.com/trentv4